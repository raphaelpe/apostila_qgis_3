# Qgis 3.4

A apostila tem o intuito de auxiliar na utilização no dia a dia do software Qgis, explicando suas funcionalidades e trazendo dicas
para melhor aproveitamento.
O conteúdo será melhorado com o passar do tempo, e contribuições são sempre bem vindas.

Os dados utilizados nessa apostila encontram-se em [https://gitlab.com/kylefelipe/dados_apostila_qgis_3](https://gitlab.com/kylefelipe/dados_apostila_qgis_3)


## Sumário
### [Introdução](conteudo/00_introducao.md)
### 1. [Instalação](conteudo/01_instalacao.md)
### 2. [Apresentação](conteudo/02_apresentacao.md)
### 3. [Mãos à Massa](conteudo/03_maosamassa.md)
#### 3.1. [Dados Vetoriais](conteudo/03_maosamassa.md#dados-vetoriais)
##### 3.1.1 [Shapefile](conteudo/03_maosamassa.md#SHAPEFILE)
##### 3.1.2 [KML](conteudo/03_maosamassa.md#KML)
##### 3.1.3 [Geopakage](conteudo/03_maosamassa.md#Geopakage)
#### 3.2 [Dados Matriciais](conteudo/03_maosamassa.md#Dados-Matriciais)
##### 3.2.1 [GeoTiff](conteudo/03_maosamassa.md#GeoTiff)
##### 3.2.2 [Geopakage](conteudo/03_maosamassa.md#Geopackage-1)
#### 3.3 [Dados Tabulares](conteudo/03_maosamassa.md#Dados-Tabulares)
##### 3.3.1 [CSV](conteudo/03_maosamassa.md#CSV)
##### 3.3.2 [Planilhas](conteudo/03_maosamassa.md#Planilhas)
##### 3.3.3 [Geopakage](conteudo/03_maosamassa.md#Geopackage-2)
### 4. [Configurando o Qgis](conteudo/04_configurando.md)
### 5. [Configurando o Projeto](conteudo/05_projeto.md)
### 6. [Complementos](conteudo/06_plugins.md)
### 7. [Adicionando arquivos](conteudo/07_adicionandoarquivos.md)
### 8. [Arquivos de Texto Delimitado](conteudo/08_textodelimitado.md)
### 9. [Criando/Editando Arquivos Vertoriais](conteudo/09_criando_editandovetores.md)
### 10. [A tabela de Atributos](conteudo/10_tabela_atributos.md)
### 11. [Calculadora de Campo](conteudo/11_calculadora_de_campo.md)
### 12. [Corrigindo erros de Geometria](conteudo/12_corrigindo_erros_geometria.md)

## [Fontes](conteudo/999_fontes.md)
